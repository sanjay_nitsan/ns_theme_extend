<?php
// TYPO3 Security Check
if (!defined('TYPO3')) {
    die('Access denied.');
}

// Let's configuration of this extension from "Extension Manager"
// $GLOBALS['TYPO3_CONF_VARS']['EXTCONF'][$_EXTKEY] = unserialize($_EXTCONF);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('@import "EXT:ns_theme_extend/Configuration/PageTSconfig/setup.typoscript"');

